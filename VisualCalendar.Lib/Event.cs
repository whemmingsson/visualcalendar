﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualCalendar.Lib
{
    public class Event
    {
        public DateTime Start { set; get; }
        public DateTime End { set; get; }
        public string Label { set; get; }

        public Event(DateTime start, DateTime end, string label)
        {
            Start = start;
            End = end;
            Label = label;
        }

        public Event(DateTime start, DateTime end) : this(start, end, string.Empty) {}
        public Event() {}

        public EventLength GetLength()
        {
            return new EventLength(End-Start);        
        }
    }
}
