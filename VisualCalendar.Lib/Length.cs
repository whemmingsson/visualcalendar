﻿using System;

namespace VisualCalendar.Lib
{
    public class EventLength
    {
        private TimeSpan _period;

        public EventLength(TimeSpan period)
        {
            _period = period;
        }

        public TimeSpan ToTimeSpan()
        {
            return _period;  
        }

        public int InMinutes()
        {
            return (int)Math.Round(_period.TotalMinutes,0);
        }

        public int InHours()
        {
            return (int)Math.Round(_period.TotalHours, 0);
        }

        public int InDays()
        {
            return (int)Math.Round(_period.TotalDays, 0);
        }
    }
}