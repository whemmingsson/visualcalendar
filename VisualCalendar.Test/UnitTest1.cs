﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VisualCalendar.Lib;

namespace VisualCalendar.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void EventGetLengthInDays()
        {
            var now = DateTime.Now;
            var someEvent = new Event() { Start = now, End = now.AddDays(1) };       
            Assert.AreEqual(1, someEvent.GetLength().InDays());
        }

        [TestMethod]
        public void EventGetLengthInHours()
        {
            var now = DateTime.Now;
            var someEvent = new Event() { Start = now, End = now.AddDays(1) };
            Assert.AreEqual(24, someEvent.GetLength().InHours());
        }

        [TestMethod]
        public void EventGetLengthInMinutes()
        {
            var now = DateTime.Now;
            var someEvent = new Event() { Start = now, End = now.AddDays(1) };
            Assert.AreEqual(24*60, someEvent.GetLength().InMinutes());
        }
    }
}
